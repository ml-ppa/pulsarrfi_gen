# Examples Directory

This directory contains files that demonstrate the functionality of the module through visual examples.

## Files

- **pulse_generation.ipynb**:  
  This notebook provides examples of using the `prfi.pulse_generator` module to create artificial pulsar pulses, represented as spectrograms.

- **RFI_generation.ipynb**:  
  This notebook showcases how to use the `prfi.rfi_generator` module to generate artificial narrowband and broadband radio interference, also represented as spectrograms.

- **noised_data_generation.ipynb**:  
  This notebook includes examples of using the `get_noised_and_normalized_array` function to add uniform Gaussian noise to artificial spectrograms, allowing for the creation of pulses and disturbances with varying amplitudes.

