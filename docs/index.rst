.. PulseRFI_Gen documentation master file, created by
   sphinx-quickstart on Tue Jul 18 15:41:40 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PulseRFI_Gen's documentation!
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
