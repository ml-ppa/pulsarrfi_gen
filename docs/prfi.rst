prfi package
============

Submodules
----------

prfi.pulse\_generator module
----------------------------

.. automodule:: prfi.pulse_generator
   :members:
   :undoc-members:
   :show-inheritance:

prfi.rfi\_generator module
--------------------------

.. automodule:: prfi.rfi_generator
   :members:
   :undoc-members:
   :show-inheritance:

prfi.utils module
-----------------

.. automodule:: prfi.utils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: prfi
   :members:
   :undoc-members:
   :show-inheritance:
