import os
import numpy as np
import pytest
from prfi.pulse_generator import Pulse


_install_dir = os.path.abspath(os.path.dirname(__file__))

def test_pulse_initialization():
    pulse = Pulse(1.0, 1.5, 1.0, 32, 1.0)
    assert pulse.DM == 1.0
    assert pulse.f_hi == 1.5
    assert pulse.f_lo == 1.0
    assert pulse.n_channels == 32
    assert pulse.t_resol == 1.0
    assert pulse.default_n_channes == 4096


@pytest.mark.parametrize('DM, f_hi, f_lo, n_channels, t_resol', [
    ('56.7', 1.53, 1.23, 32, 0.0001),
    (56.7, '1.53', 1.23, 32, 0.0001),
    (56.7, 1.53, '1.23', 32, 0.0001),
    (56.7, 1.53, 1.23, '32', 0.0001),
    (56.7, 1.53, 1.23, 32.2, 0.0001),
    (56.7, 1.53, 1.23, 32, '0.0001')
])
def test_pulse_initialization_type(DM, f_hi, f_lo, n_channels, t_resol):
    with pytest.raises(TypeError):
        Pulse(DM, f_hi, f_lo, n_channels, t_resol)


@pytest.mark.parametrize('DM, f_hi, f_lo, n_channels, t_resol', [
    (-1, 1.53, 1.23, 32, 0.0001),
    (56.7, -1.53, 1.23, 32, 0.0001),
    (56.7, 1.53, -1.23, 32, 0.0001),
    (56.7, 1.53, 1.23, -32, 0.0001),
    (56.7, 1.53, 1.23, 31, 0.0001),
    (56.7, 1.53, 1.23, 32, -0.0001),
    (56.7, 1.00, 2.00, 32, -0.0001)
])
def test_pulse_initialization_value(DM, f_hi, f_lo, n_channels, t_resol):
    with pytest.raises(ValueError):
        Pulse(DM, f_hi, f_lo, n_channels, t_resol)


@pytest.mark.parametrize('f_lo, f_hi, DM, result', [
    (1.21, 1.53, 56.758, 60.24),
    (10.0, 22.0, 1000, 32.92),
    (1.0, 2.2, 350, 1152.07),
    (.25, .50, 100, 4978.57),
])
def test_delays_DM(f_lo, f_hi, DM, result):
    delay = Pulse.delays_DM(f_lo, f_hi, DM)
    assert isinstance(delay, float)
    assert delay == result


@pytest.mark.parametrize('size', [8, 16, 32, 64,])
def test_delays_DM_list_shape(size):
    freq_list = np.linspace(1.21, 1.53, size)
    delay_list = Pulse.delays_DM_list(freq_list, 56.758)
    assert delay_list.shape == freq_list.shape


@pytest.mark.parametrize('f_lo, f_hi, DM, idx', [
    (1.21, 1.53, 56.758, 0),
    (10.0, 22.0, 1000, 1),
    (1.0, 2.2, 350, 2),
    (.25, .50, 100, 3),
])
def test_delays_DM_list_values(f_lo, f_hi, DM, idx):
    deleay_list_precalc = np.load(os.path.join(_install_dir, 'data', 'delays_array.npy'))[idx]
    delay_list = Pulse.delays_DM_list(np.linspace(f_lo, f_hi, 32), DM)
    assert delay_list.all() == deleay_list_precalc.all()


@pytest.mark.parametrize('size', [8, 16, 32, 64])
def test_gauss_type_and_shape(size):
    x = np.linspace(-10, 10, size)
    result = Pulse.gauss(x, 1.0, 2.0, 1.0)
    assert isinstance(result, np.ndarray)
    assert result.shape == x.shape

  
def test_gauss_values():
    gauss_val = Pulse.gauss(np.array([1, 2, 3]), 1, 0, 1)
    assert np.round(gauss_val, 1).all() == np.array([0.6, 0.1, 0.]).all()


def test_spectra_func_type():
    array = np.array([1, 2, 3])
    result = Pulse.spectra_func(array)
    assert isinstance(result, np.ndarray)


@pytest.mark.parametrize('test_array, correct_array', [
    ([1, 2, 3], [1., 0.32, 0.16]), 
    ([1.5, 2.5, 3.5], [0.51, 0.22, 0.13]), 
    ([10, 20, 30], [0.02, 0.01, 0.]), 
    ])
def test_spectra_func_values(test_array, correct_array):
    array = np.array(test_array)
    result = Pulse.spectra_func(array)
    assert np.round(result, 2).all() == np.array(correct_array).all()


@pytest.mark.parametrize('original_data, dispersed_data, shift', [
    ('fake_pulse_32x32.npy', 'fake_pulse_32x32_dispersed.npy', 32),
    ('fake_pulse_128x130.npy', 'fake_pulse_128x130_dispersed.npy', 128)
])
def test_do_dispersion_shape_type_and_functionality(original_data, dispersed_data, shift):
    pulse = np.load(os.path.join(_install_dir, 'data', original_data))
    dispersed_pulse = np.load(os.path.join(_install_dir, 'data', dispersed_data))
    result = Pulse.do_dispersion(pulse, np.arange(shift))
    
    assert isinstance(result, np.ndarray)
    assert result.shape == pulse.shape == dispersed_pulse.shape
    assert result.all() == dispersed_pulse.all()


@pytest.mark.parametrize('original_data, idx, f_hi, f_lo, DM, t_resol', [
    ('fake_pulse_32x32.npy', 0, 250, 200, 50, 0.001),
    ('fake_pulse_32x32.npy', 1, 150, 100, 22.7, 0.0001),
    ('fake_pulse_32x32.npy', 2, 150, 100, 22.8, 0.001),
    ('fake_pulse_32x32.npy', 3, 600, 300, 200, 0.0001),
    ('fake_pulse_128x130.npy', 4, 250, 200, 50, 0.001),
    ('fake_pulse_128x130.npy', 5, 150, 100, 22.7, 0.0001),
    ('fake_pulse_128x130.npy', 6, 150, 100, 22.8, 0.001),
    ('fake_pulse_128x130.npy', 7, 600, 300, 200, 0.0001),
])
def test_do_scattering_shape_type_and_functionality(original_data, idx, f_hi, f_lo, DM, t_resol):
    pulse = np.load(os.path.join(_install_dir, 'data', original_data))
    name, ext = original_data.split('.')
    scattered_pulse = np.load(os.path.join(_install_dir, 'data', f'{name}_scattered_{idx}.{ext}'))
    r_edge = pulse[0].size
    f_list = np.linspace(f_hi, f_lo, pulse.shape[0])
    result = Pulse.do_scattering(pulse, f_list, DM, t_resol, 0, r_edge)
    
    assert isinstance(result, np.ndarray)
    assert result.shape == pulse.shape == scattered_pulse.shape
    assert result.all() == scattered_pulse.all()


@pytest.mark.parametrize('pulses, freq_list_MHz, DM, t_resol, left_edge, right_edge', [
    ('np.zeros((5, 5))', np.ones(5), 50, 0.0001, 0, 2),
    (np.zeros(5), np.ones(5), 50, 0.0001, 0, 2),
    (np.zeros((5, 5)), 'np.ones(5)', 50, 0.0001, 0, 2),
    (np.zeros((5, 5)), np.ones((5, 5)), 50, 0.0001, 0, 2),
    (np.zeros((5, 5)), np.ones(5), '50', 0.0001, 0, 2),
    (np.zeros((5, 5)), np.ones(5), 50, '0.0001', 0, 2),
    (np.zeros((5, 5)), np.ones(5), 50, 0.0001, '0', 2),
    (np.zeros((5, 5)), np.ones(5), 50, 0.0001, 0, '2')
])
def test_do_scattering_type(pulses, freq_list_MHz, DM, t_resol, left_edge, right_edge):
    with pytest.raises(TypeError):
        Pulse.do_scattering(pulses, freq_list_MHz, DM, t_resol, left_edge, right_edge)


@pytest.mark.parametrize('freq_list_MHz, DM, t_resol, left_edge, right_edge', [
    (np.ones(6), 50, 0.0001, 0, 2),
    (np.ones(4), 50, 0.0001, 0, 2),
    (np.ones(5), -50, 0.0001, 0, 2),
    (np.ones(5), 50, 0, 0, 2),
    (np.ones(5), 50, -0.0001, 0, 2),
    (np.ones(5), 50, 0.0001, -1, 2),
    (np.ones(5), 50, 0.0001, 6, 2),
    (np.ones(5), 50, 0.0001, 0, -1),
    (np.ones(5), 50, 0.0001, 0, 6),
    (np.ones(5), 50, 0.0001, 6, 6)
])
def test_do_scattering_value(freq_list_MHz, DM, t_resol, left_edge, right_edge):
    with pytest.raises(ValueError):
        Pulse.do_scattering(np.zeros((5, 5)), freq_list_MHz, DM, t_resol, left_edge, right_edge)


@pytest.mark.parametrize('original_data, size, side', [
    ('fake_pulse_32x32.npy', 10, 'left'),
    ('fake_pulse_32x32.npy', 15, 'right'),
    ('fake_pulse_32x32.npy', 20, 'both'),
    ('fake_pulse_128x130.npy', 50, 'left'),
    ('fake_pulse_128x130.npy', 60, 'right'),
    ('fake_pulse_128x130.npy', 75, 'both'),
])
def test_add_zeros(original_data, size, side):
    pulse = np.load(os.path.join(_install_dir, 'data', original_data))
    result = Pulse.add_zeros(pulse, size, side)
    if side == 'both':
        assert result.shape == (pulse.shape[0], pulse.shape[1] + size * 2)
        assert result[:,:size].all() == result[:,-size:].all() == np.zeros(size).all()
    elif side == 'left':
        assert result.shape == (pulse.shape[0], pulse.shape[1] + size)
        assert result[:,:size].all() == np.zeros(size).all()
    elif side == 'right':
        assert result.shape == (pulse.shape[0], pulse.shape[1] + size)
        assert result[:,-size:].all() == np.zeros(size).all()
    else:
        assert result.shape == pulse.shape
        assert result.all() == pulse.all()


@pytest.mark.parametrize('array, size, side', [
    ('np.zeros((5, 5))', 10, 'left'),
    (np.zeros(5), 10, 'left'),
    (np.zeros((5, 5)), '15', 'right'),
    (np.zeros((5, 5)), 20, 5)
])
def test_add_zeros_type(array, size, side):
    with pytest.raises(TypeError):
        Pulse.add_zeros(array, size, side)


@pytest.mark.parametrize('size, side', [
    (-10, 'left'),
    (15, 'last'),
])
def test_add_zeros_value(size, side):
    with pytest.raises(ValueError):
        Pulse.add_zeros(np.zeros((5, 5)), size, side)


@pytest.mark.parametrize('n_samples, n_channels', [
    (32, 32),
    (64, 64),
    (128, 256),
    (256, 128),
])
def test_generate_pulses_type_and_shape(n_samples, n_channels):
    gauss = Pulse.gauss
    coeffs = np.random.uniform(0.5, 1, size=n_channels)

    spectrogram = Pulse.generate_pulses(gauss, 1, 1, 1, n_samples, coeffs)

    assert isinstance(spectrogram, np.ndarray)
    assert spectrogram.shape == (n_channels, n_samples)


@pytest.mark.parametrize('DM, f_hi, f_lo, n_channels, t_resol, amp, hw, loc', [
    (56.758, 1.53, 1.21, 256, 0.0001, 1, 1.0, 10),
    (20, 1.53, 1.21, 64, 0.0001, 1, 1, 5),
    (22.7, .53, .25, 32, 0.0001, 10, 2.0, 2),
    (22.8, .53, .20, 32, 0.0001, 10, 2, 2),
    (0, 1.53, 1.25, 256, 0.001, 1, 3.5, 20),
])
def test_generate_spectrogram_type_and_shape(DM, f_hi, f_lo, n_channels, t_resol, amp, hw, loc):
    pulse = Pulse(DM, f_hi, f_lo, n_channels, t_resol)
    pulse.generate_spectrogram(amp, hw, loc)

    assert isinstance(pulse.compresed_pulse, np.ndarray)
    assert pulse.compresed_pulse.shape[0] == n_channels


@pytest.mark.parametrize('amp, hw, loc', [
    ('1', 1.5, 10),
    (1.6, '1', 5.5),
    (10.5, 2, '2'),
])
def test_generate_spectrogram_type(amp, hw, loc):
    pulse = Pulse(56.758, 1.53, 1.21, 256, 0.0001)
    with pytest.raises(TypeError):
        pulse.generate_spectrogram(amp, hw, loc)


@pytest.mark.parametrize('amp, hw, loc', [
    (-1, 1.5, 10),
    (1.6, -1, 5.5),
    (10.5, 2, -2),
])
def test_generate_spectrogram_value(amp, hw, loc):
    pulse = Pulse(56.758, 1.53, 1.21, 256, 0.0001)
    with pytest.raises(ValueError):
        pulse.generate_spectrogram(amp, hw, loc)