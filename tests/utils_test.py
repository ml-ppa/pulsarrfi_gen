import os
import pytest
import numpy as np

from prfi.utils import get_noised_and_normalized_array


_install_dir = os.path.abspath(os.path.dirname(__file__))

def test_normalize_noised_array_type_shape_check_1d():
    # Test with a known array and fixed noise amplitude
    array = np.array([1, 2, 3, 4, 5])
    noise_amplitude = 0.5
    result = get_noised_and_normalized_array(array, noise_amplitude)

    # Check type
    assert isinstance(result, np.ndarray)

    # The returned array should have the same shape as the input
    assert result.shape == array.shape

    # Check that all values in the returned array are within [0, 255]
    assert np.all(result >= 0)
    assert np.all(result <= 255)

def test_normalize_noised_array_type_shape_check_2d():
    # Test with array of zeros
    array = np.zeros((5,5))
    noise_amplitude = 0.7
    result = get_noised_and_normalized_array(array, noise_amplitude)

    # Check type
    assert isinstance(result, np.ndarray)

    # The returned array should have the same shape as the input
    assert result.shape == array.shape

    # All values in the array should be still within [0, 255]
    assert np.all(result >= 0)
    assert np.all(result <= 255)

@pytest.mark.parametrize('original_data, noised_data, noise_amp, seed', [
    ('pulse.npy', 'pulse_noised.npy', 0.2, 106),
    ('bbrfi.npy', 'bbrfi_noised.npy', 0.2, 106 * 2),
    ('nbrfi.npy', 'nbrfi_noised.npy', 0.09, 106 * 3),
])
def test_normalize_noised_array_work_with_real_pulse(original_data, noised_data, noise_amp, seed):
    pulse = np.load(os.path.join(_install_dir, 'data', original_data))
    pulse_noised = np.load(os.path.join(_install_dir, 'data', noised_data))
    np.random.seed(seed) 
    result = get_noised_and_normalized_array(pulse, noise_amp)
    assert result.all() == pulse_noised.all()

def test_normalize_noised_array_type():
    invalid_array = "this is not a numpy array"
    with pytest.raises(TypeError):
        get_noised_and_normalized_array(invalid_array, 0.1)

def test_normalize_noised_array_noise_amplitude_type():
    valid_array = np.array([1, 2, 3])
    invalid_noise_amplitude = "this is not a float or an int"
    with pytest.raises(ValueError):
        get_noised_and_normalized_array(valid_array, invalid_noise_amplitude)

def test_normalize_noised_array_negative_noise_amplitude():
    valid_array = np.array([1, 2, 3])
    negative_noise_amplitude = -1
    with pytest.raises(ValueError):
        get_noised_and_normalized_array(valid_array, negative_noise_amplitude)
