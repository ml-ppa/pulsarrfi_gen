import os

import numpy as np
import pytest

from prfi.rfi_generator import RFI, NBRFI, BBRFI


_install_dir = os.path.abspath(os.path.dirname(__file__))

def test_rfi_creation_main_atributes():
    n_timesamples, n_channels = 10, 10
    rfi = RFI(n_timesamples, n_channels)
    
    assert rfi.n_timesamples == n_timesamples
    assert rfi.n_channels == n_channels
    assert (rfi.times == np.arange(n_timesamples)).all()
    assert (rfi.channels == np.arange(n_channels)).all()


def test_nbrfi_get_rfi_type_and_shape():
    nbrfi = NBRFI(10, 10)
    rfi_signal = nbrfi.get_rfi(5, 2, 1)
    
    assert isinstance(rfi_signal, np.ndarray)
    assert rfi_signal.shape == (10, 10)


def test_bbrfi_get_rfi_type_and_shape():
    bbrfi = BBRFI(10, 10)
    rfi_signal = bbrfi.get_rfi(1, 1, 1)
    
    assert isinstance(rfi_signal, np.ndarray)
    assert rfi_signal.shape == (10, 10)


def test_bbrfi_gauss():
    bbrfi = BBRFI(10, 10)
    gauss_val = bbrfi.gauss(np.array([1, 2, 3]), 1, 0, 1)
    
    assert isinstance(gauss_val, np.ndarray)
    assert gauss_val.shape == (3, )


def test_bbrfi_gauss_values():
    bbrfi = BBRFI(10, 10)
    gauss_val = bbrfi.gauss(np.array([1, 2, 3]), 1, 0, 1)
    
    assert np.round(gauss_val, 1).all() == np.array([0.6, 0.1, 0.]).all()


def test_nbrfi_get_rfi_generation():
    nbrfi = NBRFI(32, 32)
    rfi_signal = nbrfi.get_rfi(1, 3, 15)
    
    nbrfi_test = np.load(os.path.join(_install_dir, 'data', 'nbrfi.npy'))

    assert rfi_signal.all() == nbrfi_test.all()


def test_bbrfi_get_rfi_generation():
    bbrfi = BBRFI(32, 32)
    np.random.seed(106)
    rfi_signal = bbrfi.get_rfi(1, 1, 15)
    
    bbrfi_test = np.load(os.path.join(_install_dir, 'data', 'bbrfi.npy'))

    assert rfi_signal.all() == bbrfi_test.all()


@pytest.mark.parametrize('n_timesamples, n_channels', [
    ('wrong_type', 10),
    (10, 'wrong_type'),
    (10.0, 10),
    (10, 10.0)
])
def test_rfi_creation_main_atributes_invalid_type(n_timesamples, n_channels):
    with pytest.raises(TypeError):
        RFI(n_timesamples, n_channels)


@pytest.mark.parametrize('n_timesamples, n_channels', [
    (-10, 10),
    (10, -10)
])
def test_rfi_creation_main_atributes_invalid_value(n_timesamples, n_channels):
    with pytest.raises(ValueError):
        RFI(n_timesamples, n_channels)


@pytest.mark.parametrize('n_timesamples, n_channels', [
    ('wrong_type', 10),
    (10, 'wrong_type'),
    (10.0, 10),
    (10, 10.0)
])
def test_nbrfi_creation_main_atributes_invalid_type(n_timesamples, n_channels):
    with pytest.raises(TypeError):
        NBRFI(n_timesamples, n_channels)


@pytest.mark.parametrize('n_timesamples, n_channels', [
    (-10, 10),
    (10, -10)
])
def test_nbrfi_creation_main_atributes_invalid_value(n_timesamples, n_channels):
    with pytest.raises(ValueError):
        NBRFI(n_timesamples, n_channels)


@pytest.mark.parametrize('n_timesamples, n_channels', [
    ('wrong_type', 10),
    (10, 'wrong_type'),
    (10.0, 10),
    (10, 10.0)
])
def test_bbrfi_creation_main_atributes_invalid_type(n_timesamples, n_channels):
    with pytest.raises(TypeError):
        BBRFI(n_timesamples, n_channels)


@pytest.mark.parametrize('n_timesamples, n_channels', [
    (-10, 10),
    (10, -10)
])
def test_bbrfi_creation_main_atributes_invalid_value(n_timesamples, n_channels):
    with pytest.raises(ValueError):
        BBRFI(n_timesamples, n_channels)


@pytest.mark.parametrize('amp, width, freq', [
    ('wrong_type', 5, 10),
    (1, 'wrong_type', 10),
    (1, 5, 'wrong_type'),
    (1.5, 2.1, 10.5)
])
def test_nbrfi_get_rfi_invalid_type(amp, width, freq):
    nbrfi = NBRFI(32, 32)
    with pytest.raises(TypeError):
        nbrfi.get_rfi(amp, width, freq)


@pytest.mark.parametrize('amp, hw, loc', [
    ('wrong_type', 5, 10),
    (1, 'wrong_type', 10),
    (1, 5, 'wrong_type'),
    ('wrong_type', 5.5, 10.6),
    (1.6, 'wrong_type', 10.2),
    (1.7, 5.8, 'wrong_type')
])
def test_bbrfi_get_rfi_invalid_type(amp, hw, loc):
    bbrfi = BBRFI(32, 32)
    with pytest.raises(TypeError):
        bbrfi.get_rfi(amp, hw, loc)


@pytest.mark.parametrize('amp, width, freq', [
    (-1, 5, 10),
    (1, -2, 10),
    (1, 5, -10),
    (1.5, 2.1, 33)
])
def test_nbrfi_get_rfi_invalid_value(amp, width, freq):
    nbrfi = NBRFI(32, 32)
    with pytest.raises(ValueError):
        nbrfi.get_rfi(amp, width, freq)


@pytest.mark.parametrize('amp, hw, loc', [
    (-1, 5, 10),
    (1, -2, 10.5),
    (1.5, 5, -10),
    (1.5, 2.1, 32.1)
])
def test_bbrfi_get_rfi_invalid_value(amp, hw, loc):
    bbrfi = BBRFI(32, 32)
    with pytest.raises(ValueError):
        bbrfi.get_rfi(amp, hw, loc)


@pytest.mark.parametrize('x, amp, mu, sigma', [
    ('np.arange(10)', 1, 5, 10),
    (np.ones((2, 2)), 1, 5, 2),
    (np.arange(10), '1', 2, 10),
    (np.arange(10), 1.5, '5', 10),
    (np.arange(10), 1.5, 2.1, '32.1')
])
def test_bbrfi_gauss_invalid_type(x, amp, mu, sigma):
    with pytest.raises(TypeError):
        BBRFI.gauss(x, amp, mu, sigma)


@pytest.mark.parametrize('amp, mu, sigma', [
    (-1, 5, 10),
    (1, -2, 10.5),
    (1.5, 5, -10),
])
def test_bbrfi_gauss_invalid_values(amp, mu, sigma):
    with pytest.raises(ValueError):
        BBRFI.gauss(np.arange(10), amp, mu, sigma)

