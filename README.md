# Pulse Generation, Radio Frequency Interference Simulation, and Auxiliary Tools

This repository provides classes and functions to simulate radio pulse signals, Narrowband and Broadband Radio Frequency Interferences (RFI), along with a few auxiliary tools for noise addition and normalization. The developed code is built with Python and leverages numpy for mathematical operations. Numba library is used for optimization.

## Modules Overview
The codebase is divided into three modules:

+ Pulse Generation Module
+ RFI Simulation Module
+ Auxiliary Functions Module

## Pulse Simulation Module
This module includes a class for pulse generation:

Pulse class: This class is designed to simulate a pulse signal with provided parameters for amplitude, width, and center of the pulse. It provides a generate method that creates a pulse signal over a time-series.
## RFI Simulation Module
This module contains classes to simulate Narrowband and Broadband RFIs:

NBRFI class: Simulates Narrowband RFI. The RFI is generated as a Gaussian function within the frequency domain and expanded across the entire spectrogram.

BBRFI class: Simulates Broadband RFI. The RFI is generated as a Gaussian function in the time domain, extended over all or a portion of the frequency channels with greater variation of amplitude for each individual frequency channel.

Both these classes inherit from the parent RFI class that represents a generic Radio Frequency Interference.
## Auxiliary Functions Module
This module includes various auxiliary functions that are used throughout the larger codebase.

Currently, it contains the following function:

get_noised_and_normalized_array: This function normalizes an array to the range [0, 255], adds Gaussian noise, and then normalizes it again. This function can be used for testing and simulations where normalization and noise addition are required. Please note, due to the stochastic nature of the noise addition, the output will be different each time the methods are called, even with the same inputs.

### Installation
The package can be clonned from the gitlab repository as:
```
git clone https://gitlab-p4n.aip.de/punch/ta5/wp4/ml-ppa/pulsarrfi_gen.git
```
We recommend using virtual environment for running this package. This can be created in the package folder as:
```
python -m venv <venv_name>
``` 
followed by activating the virtual environment as:
```
source <venv_name>\bin\activate
```
the required packages or dependencies then can be installed in the virtual environment as:
```
pip install -r requirements.txt
```
You can then install the package by building and then installing the wheel (.whl):
```
python -m build
pip install dist/pulsarrfi_gen-0.1-py3-none-any.whl
```

## Importing
```python
from prfi_gen.pulse_generator import Pulse
from prfi_gen.rfi_generator import BBRFI, NBRFI
from prfi_gen.utils import get_noised_and_normalized_array
```

You can then create instances of NBRFI, BBRFI, and Pulse, and call their respective methods to simulate Narrowband and Broadband RFIs and generate pulses respectively.

To add noise and normalize an array, use the get_noised_and_normalized_array function from the auxiliary functions module.

*Note: If you didn't install the package you have to add the path **src** on the import*

## Usage

### Creating a spectrogram with a pulse from a pulsar:

```python
# simple pulse
DM = 56 # Close to Crab pulsar DM

f_hi = 1.53 # 1530 MHz
f_lo = 1.21 # 1210 MHz
n_channels = 256
t_resol = 0.0001 # seconds

amp = 1 # amplitude
hw = 2 # half-width of the pulse (befor scattering)
loc = 20 # position of the pulse in a window (for the first frequency channel)

pulse = Pulse(DM, f_hi, f_lo, n_channels, t_resol)
pulse.generate_spectrogram(amp, hw, loc)
noised_pulse = get_noised_and_normalized_array(pulse.compresed_pulse, 0.2)
```

Ploting the result (do not forget import [matplotlib](https://matplotlib.org/)).

```python
plt.clf()
plt.ylabel('Frequency channel')
plt.xlabel('Time samples')
plt.imshow(noised_pulse, aspect='auto', cmap=plt.get_cmap('gray'))
plt.show()
```
![a pulse example](./images/pulse_examplel.png)

### Creating a spectrogram with a BBRFI.

```python
bbrfi = BBRFI(256, 256)

amp = 1 # Amplitude of the RFI
hw_RFI = 3 # half-width of the RFI (in frequency-domain area)
location = 150 # Sequential number of time sample (location of the RFI in a window)

bbrfi_example = bbrfi.get_rfi(amp, hw_RFI, location)
noised_bbrfi = get_noised_and_normalized_array(bbrfi_example, 0.2)

plt.clf()
plt.ylabel('Frequency channel')
plt.xlabel('Time samples')
plt.imshow(noised_bbrfi, aspect='auto', cmap=plt.get_cmap('gray'))
plt.show()
```
![a pulse example](./images/bbrfi_examplel.png)

### Creating a spectrogram with a NBRFI.
```python
nbrfi = NBRFI(256, 256)

amp = 1 # Amplitude of the RFI
hw_RFI = 3 # half-width of the RFI (in frequency-domain area)
central_freq = 150 # Sequential number of the central frequency channel

nbrfi_example = nbrfi.get_rfi(amp, hw_RFI, central_freq)
noised_nbrfi = get_noised_and_normalized_array(nbrfi_example, 0.2)

plt.clf()
plt.ylabel('Frequency channel')
plt.xlabel('Time samples')
plt.imshow(noised_nbrfi, aspect='auto', cmap=plt.get_cmap('gray'))
plt.show()
```
![a pulse example](./images/nbrfi_examplel.png)
## License
Free to use GNU GENERAL PUBLIC LICENSE Version 3.

## Contact
For more information or any questions, please do not hesitate to contact **akazantsev@mpifr-bonn.mpg.de**

## Contributions
Contributions are welcome! Please make a pull request and we'll review as soon as possible.