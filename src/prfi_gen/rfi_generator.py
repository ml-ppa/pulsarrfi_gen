"""
Radio Frequency Interference (RFI) Simulation Module

This module provides two classes `NBRFI` and `BBRFI` for simulating 
Narrowband (NB) and Broadband (BB) Radio Frequency Interferences respectively. 

The `NBRFI` class generates Narrowband RFI as a Gaussian function within 
the frequency domain and expands it across the entire spectrogram.

The `BBRFI` class generates Broadband RFI as a Gaussian function in the time 
domain, extending over all or a portion of the frequency channels with greater 
variation of amplitude for each individual frequency channel.

Both `NBRFI` and `BBRFI` classes inherit from a parent `RFI` class, that 
represents a generic Radio Frequency Interference.

Each class includes methods for generating and retrieving the RFI signals 
specific to the type of RFI.

The module depends on the numpy.

"""


import numpy as np


class RFI(object):
    """
    Class representing Radio Frequency Interference (RFI).

    Parameters
    ----------
    n_timesamples : int
        Number of time samples in a spectrogram.
    n_channels : int
        Number of frequency channels in a spectrogram.
    """

    def __init__(self, n_timesamples, n_channels):
        # Check inputs are int
        if not isinstance(n_timesamples, int):
            raise TypeError('n_timesamples must be an integer')
        elif n_timesamples < 0:
            raise ValueError("n_timesamples must be nonnegative")
        
        if not isinstance(n_channels, int):
            raise TypeError('n_channels must be an integer')
        elif n_channels < 0:
            raise ValueError("n_channels must be nonnegative")
        
        self.n_timesamples = n_timesamples
        self.n_channels = n_channels
        self.times = np.arange(0, n_timesamples)
        self.channels = np.arange(0, n_channels)


class NBRFI(RFI):
    """
    Class representing Narrowband RFI.

    Inherits from the RFI class.

    NBRFI is generated as a Gaussian function within 
    the frequency domain and expanded across the entire spectrogram. 
    """

    def get_rfi(self, amp, width, freq):
        """
        Method to get Narowband RFI.
        
        Parameters
        ----------
        amp : int or float
            Amplitude of the RFI.
        width : int or float
            Half-width of the RFI (in frequency-domain area).
        freq : int
            Central frequency channel of the RFI.


        Returns
        -------
        numpy.ndarray
            RFI signal.
        """

        # Check inputs are either int or float
        if not isinstance(amp, (int, float)):
            raise TypeError('amp must be an integer or float')
        elif amp < 0:
            raise ValueError("amp must be nonnegative")
        
        if not isinstance(width, (int, float)):
            raise TypeError('width must be an integer or float')
        elif width < 0:
            raise ValueError("width must be nonnegative")
        
        if not isinstance(freq, int):
            raise TypeError('freq must be an integer')
        elif not (0 <= freq <= self.n_channels):
            raise ValueError("freq must be nonnegative and equal or less then n_channels")

        _, F = np.meshgrid(self.times, self.channels)
        return amp * np.exp(-((F - freq) ** 2) / (2 * width**2))

class BBRFI(RFI):
    """
    Class representing Broadband RFI.

    Inherits from the RFI class.

    BBRFI is generated as a Gaussian function in the time domain, 
    extended over all or a portion of the frequency channels with 
    greater variation of amplitude for each individual frequency channel.
    """

    @staticmethod
    def gauss(x, amp, mu, sigma):
        """
        Gaussian function.
        
        Parameters
        ----------
        x : numpy.ndarray
            Input array.
        amp : int or float
            Amplitude of the gaussian function.
        mu : int or float
            Mean of the gaussian function.
        sigma : int or float
            Standard deviation of the gaussian function.
            
        Returns
        -------
        numpy.ndarray
            Gaussian function.
        """

        if not isinstance(x, np.ndarray) or x.ndim != 1:
            raise TypeError("x must be a 1-dimensional numpy.ndarray")
        
        if not isinstance(amp, (int, float)):
            raise TypeError('Amplitude must be an integer or a float')
        elif amp < 0:
            raise ValueError('amp must be nonnegative')

        if not isinstance(mu, (int, float)):
            raise TypeError('Mmu must be an integer or a float')
        elif mu < 0:
            raise ValueError('mu must be nonnegative')
        
        if not isinstance(sigma, (int, float)):
            raise TypeError('sigma must be an integer or a float')
        elif sigma < 0:
            raise ValueError('sigma must be nonnegative')

        return amp * np.exp(-((x - mu)**2) / (2.0 * sigma**2))
    
    def get_rfi(self, amp, hw, loc):
        """
        Method to get Broadband RFI.
        
        Parameters
        ----------
        amp : int or float
            Amplitude of the RFI.
        hw : int or float
            Half-width of the RFI.
        loc : int or float
            Location (in time=domain area) of the RFI.
            
        Returns
        -------
        numpy.ndarray
            RFI signal.
        """

        # Check inputs are either int or float
        if not isinstance(amp, (int, float)):
            raise TypeError('amp must be an integer or float')
        elif amp < 0:
            raise ValueError("amp must be nonnegative")
        
        if not isinstance(hw, (int, float)):
            raise TypeError('hw must be an integer or float')
        elif hw < 0:
            raise ValueError("hw must be nonnegative")
        
        if not isinstance(loc, (int, float)):
            raise TypeError('loc must be an integer or float')
        elif not (0 <= loc <= self.n_timesamples):
            raise ValueError("freq must be nonnegative and equal or less then n_timesamples")
        
        
        


        rfi = np.empty((self.n_channels, self.n_timesamples))

        for i in range(self.n_channels):
            rfi[i] = self.gauss(
            np.arange(self.n_timesamples),
            amp * np.random.uniform(0, 1) * np.random.uniform(0, 1),
            loc,
            hw,
        ) * np.random.uniform(0, 1, self.n_timesamples)
            
        return rfi
