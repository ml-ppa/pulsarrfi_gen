"""
Auxiliary Functions Module

This module contains various auxiliary functions that are used throughout the 
larger codebase. 


- `get_noised_and_normalized_array`: Normalizes an array to the range [0, 255], 
  adds Gaussian noise, and then normalizes again.

  
The module depends on the numpy.
"""


import numpy as np


def get_noised_and_normalized_array(array, noise_amplitude):
    """
    Normalize an array to the range [0, 255], add Gaussian noise, and then normalize again.

    This function first normalizes the input array to the range [0, 1]. It then adds Gaussian noise
    with mean 0 and a user-specified standard deviation. The noisy array is shifted to be nonnegative
    by adding the absolute value of its minimum. Finally, the array is scaled to the range [0, 255].

    Parameters
    ----------
    array : np.array
        The input array to be noised and normalized.
    noise_amplitude : float
        The standard deviation of the Gaussian noise to be added.

    Returns
    -------
    np.array
        The noised and normalized array.

    Notes
    -----
    Due to the random nature of the added noise, the output of this function will be different
    each time it's called, even with the same inputs.
    """

    # Type checking
    if not isinstance(array, np.ndarray):
        raise TypeError("Expected array to be a numpy.ndarray")

    # Value checking
    if not isinstance(noise_amplitude, (float, int)):
        raise ValueError("Expected noise_amplitude to be a float or an int")
    if noise_amplitude < 0:
        raise ValueError("Noise_amplitude must be nonnegative")

    img = array / (np.max(array) + 1e-10)
    noise = np.random.normal(loc=0.0, scale=noise_amplitude, size=array.shape)
    img = img + noise
    img += abs(np.min(img))
    img = (img / (np.max(img) + 1e-10)) * 255
    
    return img