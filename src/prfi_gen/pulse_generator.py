"""
Pulse Simulation Module

This module provides the class `Pulse` to generate and manipulate individual pulse signals 
including the simulation of the effects of dispersion and scattering of the pulse. These are
commonly seen in radio astronomy and pulsar science.

The `Pulse` class includes the following methods:

- `__init__`: Initializes the class with parameters like Dispersion Measure (DM), upper and lower frequency limits, number of frequency channels and time resolution for the simulated observation.

- `delays_DM`: Calculates delay due to interstellar dispersion.

- `delays_DM_list`: Calculates a list of delays for a list of frequencies due to interstellar dispersion.

- `do_dispersion`: Applies dispersion to the pulse signal.

- `gauss`: Gaussian function to mimic an individual pulse shape.

- `spectra_func`: Calculates power law spectra for the input array following a power law with an index of -1.65, as observed in pulsars.

- `do_scattering`: Simulates interstellar scattering of the pulses.

- `generate_pulses`: Generates a set of simulated pulses based on a Gaussian function.

- `generate_spectrogram`: Generates a spectrogram for a given set of pulse parameters.

- `add_zeros`: Add zero-padding to the specified side of the given spectrogram..

It uses Numba's Just-In-Time (JIT) compilation decorator (@njit) for optimizing the 
computations and for enabling parallel computing capabilities. Additionally, Numba's prange 
is used for explicit parallel loop.

The module depends on the numpy and numba libraries.

Note: The methods and attributes within the Pulse class can be directly accessed via the 
class instance.
"""


import numpy as np
from numba import njit, prange


class Pulse(object):
    """
    A class for generating and manipulating individual pulse signals, including simulating 
    the effects of dispersion and scattering of the pulse.

    Parameters
    ----------
    DM : float
        Dispersion Measure, in cm-3pc.
    f_hi : float
        Upper frequency limit, in GHz.
    f_lo : float
        Lower frequency limit, in GHz.
    n_channels : int
        Number of frequency channels in the simulated observation.
    t_resol : float
        Time resolution of the simulated observation, in seconds.
    """
    
    def __init__(self, DM, f_hi, f_lo, n_channels, t_resol):
        if not isinstance(DM, (int, float)):
            raise TypeError('DM must be an integer or float')
        elif DM < 0:
            raise ValueError("DM must be nonnegative")

        if not isinstance(f_hi, (int, float)):
            raise TypeError('f_hi must be an integer or float')
        elif f_hi < 0:
            raise ValueError("f_hi must be nonnegative")

        if not isinstance(f_lo, (int, float)):
            raise TypeError('f_lo must be an integer or float')
        elif f_lo < 0 or f_hi < f_lo:
            raise ValueError("f_lo must be nonnegative and lower than f_hi")

        if not isinstance(n_channels, int):
            raise TypeError('n_channels must be an integer')
        elif n_channels < 32:
            raise ValueError("n_channels must be equal or higher than 32")

        if not isinstance(t_resol, (int, float)):
            raise TypeError('t_resol must be an integer or float')
        elif t_resol < 0:
            raise ValueError("t_resol must be nonnegative")
        
        self.DM = DM  # cm-3pc
        self.f_hi = f_hi  # GHz
        self.f_lo = f_lo  # GHz
        self.n_channels = n_channels  # int
        self.t_resol = t_resol  # seconds
        self.default_n_channes = 4096

    @staticmethod
    @njit()
    def delays_DM(f_lo, f_hi, DM):
        """
        Calculates delay due to interstellar dispersion.
        
        delta t = 4.148808 ms * [(f_lo)^-2[GHz] -  (f_hi)^-2[GHz]] * DM[cm-3pc]

        Parameters
        ----------
        f_lo : float
            Lower frequency limit, in GHz.
        f_hi : float
            Upper frequency limit, in GHz.
        DM : float
            Dispersion Measure, in cm-3pc.

        Returns
        -------
        float
            Delay in time [ms], rounded to 2 decimal places.
        """

        return round(4.148808 * ((f_lo) ** -2 - (f_hi) ** -2) * DM, 2)

    @staticmethod
    @njit(parallel=True)
    def delays_DM_list(f_list, DM):
        """
        Calculates list of delays for a list of frequencies due to interstellar dispersion.

        Parameters
        ----------
        f_list : array_like
            List of frequencies, in GHz.
        DM : float
            Dispersion Measure, in cm-3pc.

        Returns
        -------
        ndarray
            Array of calculated delays.
        """

        dt_list = np.zeros(f_list.shape[0])
        constant = 4.148808 * DM
        for i in prange(f_list.shape[0]):
            t1 = constant * f_list[0] ** (-2)
            t2 = constant * f_list[i] ** (-2)
            dt_list[i] = round(t1 - t2, 1)
        return dt_list

    @staticmethod
    @njit(parallel=True)
    def do_dispersion(array, delays_list):
        """
        Applies dispersion to the pulse signal.

        Parameters
        ----------
        array : ndarray
            Array of pulse signals.
        delays_list : ndarray
            Array of delays to apply.

        Returns
        -------
        ndarray
            Array of dispersed pulse signals.
        """

        dispersed_pulse = np.empty_like(array)
        for i in prange(array.shape[0]):
            dispersed_pulse[i] = np.roll(array[i], -int(delays_list[i]))
        return dispersed_pulse

    @staticmethod
    @njit()
    def gauss(x, amp, mu, sigma):
        """
        Gaussian function to mimic an individual pulse shape.
        
        Parameters
        ----------
        x : numpy.ndarray
            Input array aka a time series.
        amp : int or float
            Amplitude of the gaussian function.
        mu : int or float
            Mean of the gaussian function.
        sigma : int or float
            Standard deviation of the gaussian function.
            
        Returns
        -------
        numpy.ndarray
            Gaussian function.
        """

        return amp * np.exp(-((x - mu)**2) / (2.0 * sigma**2))

    @staticmethod
    @njit()
    def spectra_func(array):
        """
        Calculates power law spectra for the input array following a power law
        with an index of -1.65, as observed in pulsars.
        
        Reference: 
        F. Jankowski, et al, MNRAS, Volume 473, Issue 4, February 2018, Pages 4436–4458,
        https://doi.org/10.1093/mnras/stx2476

        Parameters
        ----------
        array : ndarray
            Array for which spectra is to be calculated.

        Returns
        -------
        ndarray
            Calculated power law spectra.
        """

        return array**-1.65

    @staticmethod
    def do_scattering(pulses, freq_list_MHz, DM, t_resol, left_edge, right_edge):
        """
        Simulates interstellar scattering of the pulses.

        This is done through a process of FFT convolution. The scattering timescale
        is taken from two models based on the DM of the pulse. This effect is then
        applied to the input pulses. 

        The scattering kernel is normalized before FFT convolution.

        References:
        Siyao Xu and Bing Zhang, 2017, ApJ, 835, 2, DOI: 10.3847/1538-4357/835/1/2
        F. Kirsten et al, 2019, ApJ, 874, 179, DOI: 10.3847/1538-4357/ab0c05

        Parameters
        ----------
        pulses : ndarray
            A spectrogram.
        freq_list_MHz : ndarray
            Array of frequency values in MHz.
        DM : float
            Dispersion Measure, in cm-3pc.
        t_resol : float
            Time resolution, in seconds.
        left_edge : int
            Index of the left edge of the region where scattering is applied.
        right_edge : int
            Index of the right edge of the region where scattering is applied.

        Returns
        -------
        ndarray
            Scattered pulses.
        """
        
        # Type and value checking
        if not isinstance(pulses, np.ndarray) or pulses.ndim != 2:
            raise TypeError("pulses must be a 2-dimensional numpy.ndarray")

        if not isinstance(freq_list_MHz, np.ndarray)  or freq_list_MHz.ndim != 1:
            raise TypeError("freq_list_MHz must be a 1-dimensional numpy.ndarray")
        elif freq_list_MHz.shape[0] != pulses.shape[0]:
            raise ValueError("freq_list_MHz.shape[0] and pulses.shape[0] must be equal")
        
        if not isinstance(DM, (int, float)):
            raise TypeError("DM must be a number")
        elif DM < 0:
            raise ValueError("DM must be non-negative")

        if not isinstance(t_resol, (int, float)):
            raise TypeError("t_resol must be a number")
        elif t_resol <= 0:
            raise ValueError("t_resol must be greater than zero")

        if not isinstance(left_edge, int):
            raise TypeError("left_edge must be an integer")
        elif left_edge < 0 or left_edge >= pulses.shape[1]:
            raise ValueError("left_edge must be within the bounds of the pulses array")

        if not isinstance(right_edge, int):
            raise TypeError("right_edge must be an integer")
        elif right_edge <= left_edge or right_edge > pulses.shape[1]:
            raise ValueError("right_edge must be within the bounds of the pulses array, and greater than left_edge")

        def heaviside(x):
            return 1 if x >= 0 else 0
        
        left_zone, pulses_zone, right_zone = (
            pulses[:, :left_edge],
            pulses[:, left_edge:right_edge],
            pulses[:, right_edge:],
        )
        
        win_size = len(pulses_zone[0])
        t = np.linspace(0, win_size - 1, win_size)
        heaviside_t = np.array([heaviside(ti) for ti in t])

        coeff = 1 / 1000 / t_resol
        scattered_pulses = np.empty_like(pulses_zone)

        # https://iopscience.iop.org/article/10.3847/1538-4357/835/1/2
        # Siyao Xu and Bing Zhang, 2017, ApJ, 835, 2, DOI: 10.3847/1538-4357/835/1/2
        # low DM
        # t_s [ms] = 3.5 \cdot 10^{5} DM^{2.2} [pc cm-3] \mu^{-4.4} [MHz]
        # high DM (> 22.7)
        # t_s [ms] = 6.6 \cdot 10^{12} DM^{4.2} [pc cm-3] \mu^{-8.4} [MHz]
        t_scattering_ms = (
            3.5e5 * DM**2.2 * freq_list_MHz**-4.4
            if DM <= 22.7
            else 6.6e12 * DM**4.2 * freq_list_MHz**-8.4
        )
        t_scattering_point = t_scattering_ms * coeff
        
        # https://iopscience.iop.org/article/10.3847/1538-4357/ab0c05
        # F. Kirsten et al, 2019, ApJ, 874, 179, DOI: 10.3847/1538-4357/ab0c05
        # u(t) = \frac{1}{t_{scattering}} e^{-\frac{t}{t_{scattering}}} H(t)
        
        kernel_common = np.exp(-t / t_scattering_point[:, np.newaxis]) * heaviside_t
        kernel_common = kernel_common / kernel_common.sum(axis=1, keepdims=True)
        
        # FFT of kernel
        F2 = np.fft.fft(kernel_common, axis=1)

        for i in range(freq_list_MHz.size):
            # FFT convolution
            F1 = np.fft.fft(pulses_zone[i])
            scattered_pulses[i] = np.fft.ifft(F1 * F2[i])[:win_size].real

        return np.concatenate((left_zone, scattered_pulses, right_zone), axis=1)

    @staticmethod
    @njit(parallel=True)
    def generate_pulses(gauss, amp, hw, loc, size, coeffs):
        """
        Generates a set of simulated pulses based on a Gaussian function.

        Each pulse is a Gaussian with its own amplitude, center, and width. The amplitudes
        are scaled by random multipliers and the widths are adjusted with random multipliers.
        
        Parameters
        ----------
        gauss : function
            Function to generate a Gaussian pulse.
        amp : float
            Amplitude of the Gaussian pulse.
        hw : float
            Half-width of the Gaussian pulse.
        loc : int
            Location of the center of the Gaussian pulse.
        size : int
            Size of the array to generate.
        coeffs : ndarray
            Array of coefficients for adjusting the amplitudes of the pulses.

        Returns
        -------
        ndarray
            Array of generated pulses.
        """

        pattern_pulses = np.empty((coeffs.shape[0], size))

        hw_randoms = np.random.uniform(0.5, 1, size=coeffs.shape[0])
        random_multipliers = np.random.uniform(0, 1, size=coeffs.shape[0])
        coeffs = random_multipliers * coeffs

        for i in prange(pattern_pulses.shape[0]):
            pattern_pulses[i] = gauss(np.arange(size), amp*coeffs[i], loc, hw * hw_randoms[i]) 

        return pattern_pulses

    @staticmethod
    def add_zeros(spectrogram, size, side):
        """
        Add zero-padding to the specified side of the given spectrogram.

        Parameters
        ----------
        spectrogram : np.array
            The original spectrogram to which the zero-padding will be added.

        size : int
            The number of zero-columns to add.

        side : str
            Specifies which side(s) of the spectrogram to add the padding.
            This should be one of the following: 'both', 'left', 'right'.
            If 'both', the padding is added on both sides.
            If 'left', the padding is added only to the left side.
            If 'right', the padding is added only to the right side.
            Any other value will return the original spectrogram without padding.

        Returns
        -------
        np.array
            The zero-padded spectrogram.
        """
        # Type and value checking
        
        if not isinstance(spectrogram, np.ndarray) or spectrogram.ndim != 2:
            raise TypeError("spectrogram must be a 2-dimensional numpy.ndarray")

        if not isinstance(size, int):
            raise TypeError("size must be an integer")
        elif size < 0:
            raise ValueError("size must be non-negative")

        if not isinstance(side, str):
            raise TypeError("side must be a string")
        elif side not in ['both', 'left', 'right']:
            raise ValueError("side must be one of 'both', 'left', or 'right'")
        
        zeros = np.zeros((spectrogram.shape[0], int(round(size, 0))))
        if side == 'both':
            return np.concatenate((zeros, spectrogram, zeros), axis=1)
        elif side == 'left':
            return np.concatenate((zeros, spectrogram), axis=1)
        elif side == 'right':
            return np.concatenate((spectrogram, zeros), axis=1)

    def generate_spectrogram(self, amplitude, hw, location):
        """
        Generate the spectrogram for a given parametrs of the pulse.

        Parameters
        ----------
        amplitude : float
            Amplitude of the pulse.
        hw : float
            Half-width (sigma) of the pulse.
        location : int
            The location where the pulse starts.

        Returns
        -------
        None

        Notes
        -----
        This method also modifies the `compresed_pulse` attribute of the class instance.
        """

        if not isinstance(amplitude, (int, float)):
            raise TypeError('amplitude must be an integer or float')
        elif amplitude < 0:
            raise ValueError("amplitude must be nonnegative")

        if not isinstance(hw, (int, float)):
            raise TypeError('hw must be an integer or float')
        elif hw < 0:
            raise ValueError("hw must be nonnegative")

        if not isinstance(location, (int, float)):
            raise TypeError('location must be an integer or float')
        elif location < 0:
            raise ValueError("location must be nonnegative")

        freq_list = np.linspace(self.f_hi, self.f_lo, self.default_n_channes)
        delays_list = self.delays_DM_list(freq_list, self.DM)
        window_size_point = int(
            round(
                self.delays_DM(self.f_lo, self.f_hi, self.DM) / 1000 / self.t_resol, 0
            )
        )
        delays_list_point = delays_list / self.t_resol / 1000

        t_scattering_ms = (
            3.5e5 * self.DM**2.2 * (self.f_lo * 1000) ** -4.4
            if self.DM <= 22.7
            else 6.6e12 * self.DM**4.2 * (self.f_lo * 1000) ** -8.4
        )

        t_scattering_point = t_scattering_ms / 1000 / self.t_resol

        window_size_point += int(location + hw * 2 + t_scattering_point * 4) # abour this four coeff see comments below

        spectr = self.spectra_func(freq_list)
        coeffs = (spectr / max(spectr))

        pattern_pulse = self.generate_pulses(
            self.gauss, amplitude, hw, location, window_size_point, coeffs
        )

        if t_scattering_point > 1:

            freq_list_MHz = freq_list * 1000
            scattered_pulse = self.do_scattering(
                pattern_pulse,
                freq_list_MHz,
                self.DM,
                self.t_resol,
                0 if int(location - 3 * hw) < 0 else int(location - 3 * hw),
                int(location + 3 * hw + t_scattering_point * 4),
            )
            # The coefficient 4 is used here to take into account the length of the tail of the scattering. 
            # In general, for a Gaussian function this value is about 2.35, but we take it with a reserve, 
            # so as not to cut the tail going into noise. 
        else:
            scattered_pulse = pattern_pulse

        dispersed_pulse = self.do_dispersion(scattered_pulse, delays_list_point)

        subpass = int(self.default_n_channes / self.n_channels)

        channels_borders = [
            (i * subpass, (i + 1) * subpass) for i in range(self.n_channels)
        ]
        self.compresed_pulse = np.empty((self.n_channels, window_size_point))
        for idx, borders in enumerate(channels_borders):
            self.compresed_pulse[idx] = np.sum(
                dispersed_pulse[borders[0] : borders[1]], axis=0
            )


if __name__ == "__main__":
    DM = 256.758

    f_hi = .53
    f_lo = .31
    n_channels = 256
    t_resol = 0.0001024  # seconds

    pulse = Pulse(DM, f_hi, f_lo, n_channels, t_resol)
    pulse.generate_spectrogram(1, 50, 50)  # amplitude, sigma, location
